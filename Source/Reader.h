/*
  ==============================================================================

    Reader.h
    Created: 4 Dec 2013 2:07:18am
    Author:  Jennifer Hsu

  ==============================================================================
*/

#ifndef READER_H_INCLUDED
#define READER_H_INCLUDED

#include "definitions.h"


class Reader
{
    
public:
    
    // constructors/destructor
    Reader( );
    ~Reader( );
    
    // modify parameters
    void increment( );
    void setStartSample( int samp );
    void setCurrSample( int samp );
    void setDurSamples( int durSamples );
    void setMaxSample( int maxSample );
    void prepNewChunk( int maxVal );
    void setWeight0( float weight0 );
    void setWeight1( float weight1 );
    void setActiveState( int activeState );
    void setEnvState( int envState );
    void setForwardRevState( int forwardRevState );
    
    // get parameters
    int getCurrSample( );
    int getDurSamples( );
    int getStartSample( );
    int getCounter( );
    float getWeight0( );
    float getWeight1( );
    int getActiveState( );
    int getEnvState( );
    
private:
    int m_durSamples;
    int m_counter;
    int m_currSample;
    int m_maxSample;
    int m_startSample;
    float m_weight0;
    float m_weight1;
    int m_activeState;
    int m_envState;
    int m_forwardRevState;
};




#endif  // READER_H_INCLUDED
