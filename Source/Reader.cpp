/*
  ==============================================================================

    Reader.cpp
    Created: 4 Dec 2013 2:07:18am
    Author:  Jennifer Hsu

  ==============================================================================
*/

#include "Reader.h"

Reader::Reader( )
{
    m_durSamples = 0;
    m_counter = 0;
    m_startSample = 0;
    m_currSample = 0;
    m_maxSample = 44100;
    m_weight0 = 0.5f;
    m_weight1 = 0.5f;
    m_activeState = INACTIVE;
    m_forwardRevState = FORWARD;
}

Reader::~Reader( )
{
    
}

void Reader::increment( )
{
    if(m_activeState == ACTIVE)
    {
        m_counter++;
        if(m_forwardRevState == FORWARD)
        {
            m_currSample--;
        } else
        {
            // reverse
            m_currSample++;
        }
    
        while(m_currSample < 0)
            m_currSample += m_maxSample;
        while(m_currSample >= m_maxSample)
            m_currSample -= m_maxSample;
    
        if(m_counter >= m_durSamples)
        {
            m_activeState = INACTIVE;
        }
    }
}

void Reader::setStartSample( int samp )
{
    m_startSample = samp;
}

void Reader::setCurrSample( int samp )
{
    m_currSample = samp;
}

void Reader::setDurSamples( int durSamples )
{
    m_durSamples = durSamples;
}

void Reader::setMaxSample( int maxSample )
{
    m_maxSample = maxSample;
}

void Reader::prepNewChunk( int newStartSample )
{
    m_startSample = newStartSample;
    while(m_startSample < 0)
        m_startSample += m_maxSample;
    while(m_startSample >= m_maxSample )
        m_startSample -= m_maxSample;
    
    m_currSample = m_startSample;
    m_counter = 0;
}

void Reader::setWeight0( float weight0 )
{
    m_weight0 = weight0;
}

void Reader::setWeight1( float weight1 )
{
    m_weight1 = weight1;
}

void Reader::setActiveState( int activeState )
{
    m_activeState = activeState;
}

void Reader::setEnvState( int envState )
{
    m_envState = envState;
}

void Reader::setForwardRevState(int forwardRevState)
{
    m_forwardRevState = forwardRevState;
}

int Reader::getCurrSample( )
{
    return m_currSample;
}

int Reader::getDurSamples( )
{
    return m_durSamples;
}

int Reader::getStartSample( )
{
    return m_startSample;
}

int Reader::getCounter( )
{
    return m_counter;
}

float Reader::getWeight0( )
{
    return m_weight0;
}

float Reader::getWeight1( )
{
    return m_weight1;
}

int Reader::getActiveState( )
{
    return m_activeState;
}

int Reader::getEnvState( )
{
    return m_envState;
}