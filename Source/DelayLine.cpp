/*
  ==============================================================================

    DelayLine.cpp
    Created: 23 Nov 2013 5:48:47pm
    Author:  Jennifer Hsu
    
    class that implements a fractional delay line

  ==============================================================================
*/



#include "DelayLine.h"
#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <cmath>

DelayLine::DelayLine( )
{
    m_fs = 0.0f;
    
    m_delTimeTarget = 1.0f;
    m_delSamplesTarget = ( int )m_fs*m_delTimeTarget;
    m_delSamplesCurrent = m_delSamplesTarget;
    m_delayIncrement = 0.0f;
    
    m_writer = MAX_DELAY-1;
    
    m_wetdryTarget = 1.0f;
    m_wetdryCurrent = 1.0f;
    m_wetdryIncrement = 0.0f;
    
    m_feedbackTarget = 0.0f;
    m_feedbackCurrent = 0.0f;
    m_feedbackIncrement = 0.0f;
    
    m_spread = 0.0f;
    
    m_minInteronsetTime = 0.75f;
    m_maxInteronsetTime = 0.75f;
    
    m_delBuffer = new float[MAX_DELAY];
    m_readers = new Reader[MAX_READERS];
    m_numReaders = 4;
    
    // hardcode initial values
    int grainDur = 44100;
    m_grainDurTarget = 1.0f;
    m_grainDurSamplesTarget = 44100;
    m_grainDurSamplesCurrent = 44100.0f;
    m_grainDurIncrement = 0.0f;
    
    // hardcode initial values
    m_interonsetTimeTarget = 0.75f*m_grainDurTarget;
    m_interonsetSamplesTarget = 33075;
    m_interonsetSamplesCurrent = 33075.0f;
    m_interonsetSamplesIncrement = 0.0f;
    
    m_grainDelaySamples = 13230;
    
    
    for(int i = 0; i < m_numReaders; i++)
    {
        m_readers[i].setDurSamples(grainDur);
        m_readers[i].setStartSample(m_grainDelaySamples);
        m_readers[i].setCurrSample(m_grainDelaySamples);
        m_readers[i].setMaxSample( MAX_DELAY );
    }
    
    m_counter = 0;
    m_interonsetCounter = m_interonsetSamplesTarget;
    if(m_numReaders > 0)
    {
        m_readers[0].setActiveState( ACTIVE );
    }
    
    m_reverse = 0.0f;
    
    m_snapMode = SNAP_OFF;
    
    m_randomness = 0.0f;
    
}


DelayLine::DelayLine( float fs, float delTime )
{
    m_fs = fs;
    
    m_delTimeTarget = delTime;
    m_delSamplesTarget = ( int )m_fs*m_delTimeTarget;
    m_delSamplesCurrent = m_delSamplesTarget;
    m_delayIncrement = 0.0f;
    
    m_writer = MAX_DELAY - 1;
    
    m_wetdryTarget = 1.0f;
    m_wetdryCurrent = 1.0f;
    m_wetdryIncrement = 0.0f;
    
    m_feedbackTarget = 0.0f;
    m_feedbackCurrent = 0.0f;
    m_feedbackIncrement = 0.0f;
    
    m_spread = 0.0f;
    
    m_minInteronsetTime = 0.75f;
    m_maxInteronsetTime = 0.75f;
    
    m_delBuffer = new float[MAX_DELAY];
    m_readers = new Reader[MAX_READERS];
    m_numReaders = 4;
    
    // just test this out for now
    int grainDur = 44100;
    m_grainDurTarget = 1.0f;
    m_grainDurSamplesTarget = 44100;
    m_grainDurSamplesCurrent = 44100.0f;
    m_grainDurIncrement = 0.0f;
    
    // hardcode for now
    m_interonsetTimeTarget = 0.75f*m_grainDurTarget;
    m_interonsetSamplesTarget = 33075;
    m_interonsetSamplesCurrent = 33075.0f;
    m_interonsetSamplesIncrement = 0.0f;
    
    m_grainDelaySamples = 13230;
    
    
    for(int i = 0; i < m_numReaders; i++)
    {
        m_readers[i].setDurSamples(grainDur);
        m_readers[i].setStartSample(m_grainDelaySamples);
        m_readers[i].setCurrSample(m_grainDelaySamples);
        m_readers[i].setMaxSample( MAX_DELAY );
    }
    
    m_counter = 0;
    m_interonsetCounter = m_interonsetSamplesTarget;
    if(m_numReaders > 0)
    {
        m_readers[0].setActiveState( ACTIVE );
    }
    
    m_reverse = 0.0f;
    
    m_snapMode = SNAP_OFF;
    
    m_randomness = 0.0f;
}

DelayLine::~DelayLine( )
{
    delete [ ] m_delBuffer;
    delete [ ] m_readers;
}

void DelayLine::setSampleRate( float fs )
{
    m_fs = fs;
    m_delSamplesTarget = ( int )m_fs*m_delTimeTarget;
}

void DelayLine::setWetDry( float wd )
{
    m_wetdryTarget = wd;
}

void DelayLine::setFeedback( float fb )
{
    m_feedbackTarget = fb;
}

void DelayLine::setDelayTime( float delTime )
{
    m_delTimeTarget = delTime;
    m_delSamplesTarget = ( int )m_fs*m_delTimeTarget;
}


void DelayLine::setGrainLength(float grainLength, float sampleRate)
{
    m_fs = sampleRate;
    m_grainDurTarget = grainLength;
    m_grainDurSamplesTarget = ( int )floor(grainLength*sampleRate);
}

void DelayLine::setSpread(float spreadVal)
{
    m_spread = spreadVal;
}

void DelayLine::setMinInteronsetTime( float minInteronsetTime )
{
    m_minInteronsetTime = minInteronsetTime;
    // force max interonset time to be equal if it's lower
    if(m_maxInteronsetTime < m_minInteronsetTime)
        m_maxInteronsetTime = minInteronsetTime;
}

void DelayLine::setMaxInteronsetTime( float maxInteronsetTime )
{
    m_maxInteronsetTime = maxInteronsetTime;
    // force min interonset time to be equal if it's higher
    if(m_maxInteronsetTime < m_minInteronsetTime)
        m_minInteronsetTime = m_maxInteronsetTime;
}

void DelayLine::setGrainDelay( float grainDelay )
{
    m_grainDelaySamples = ( int )m_fs*grainDelay;
}

void DelayLine::setReverse(float reverse)
{
    m_reverse = reverse;
}

void DelayLine::setRandomness( float randomness )
{
    m_randomness = randomness;
}

void DelayLine::setSnap( int snapMode )
{
    m_snapMode = snapMode;
    if(snapMode == SNAP_OFF)
    {
        m_numReaders = 4;
    } else
    {
        // snap on
        m_numReaders = 2;
    }
}

void DelayLine::setInteronsetCounter( int cnt )
{
    m_interonsetCounter = cnt;
}

// to be called in block processing before calling the tick function!
void DelayLine::calculateIncrements(int blockSize)
{
    float oneoverblocksize = 1.0f/(float)blockSize;
    
	// calculate the delay time in samples
	m_delayIncrement = (m_delSamplesTarget - m_delSamplesCurrent) * oneoverblocksize;
    m_feedbackIncrement = (m_feedbackTarget - m_feedbackCurrent) * oneoverblocksize;
    m_grainDurIncrement = (m_grainDurSamplesTarget - m_grainDurSamplesCurrent) * oneoverblocksize;
    m_wetdryIncrement = (m_wetdryTarget - m_wetdryCurrent) * oneoverblocksize;
    
    m_interonsetSamplesIncrement = (m_interonsetSamplesTarget - m_interonsetSamplesCurrent) * oneoverblocksize;
}

float DelayLine::getSampleRate( )
{
    return m_fs;
}

float DelayLine::getWetDry( )
{
    return m_wetdryCurrent;
}

float DelayLine::getFeedback( )
{
    return m_feedbackCurrent;
}

float DelayLine::getDelayTime( )
{
    return ( float )m_delSamplesCurrent / m_fs;
}

void DelayLine::tick( float * inSamp, float * outSamp0, float * outSamp1 )
{
    float playposfloat, playposfrac;
    long playpos, playposp1;
    
    
    // --- read from delay line ---
    
    // update delay samples, fb for smooth changes
    m_delSamplesCurrent += m_delayIncrement;
    m_feedbackCurrent += m_feedbackIncrement;
    m_wetdryCurrent += m_wetdryIncrement;
    m_grainDurSamplesCurrent += m_grainDurIncrement;
    m_interonsetSamplesCurrent += m_interonsetSamplesIncrement;
    
    float newSamp = 0.0f;
    float outsample0 = 0.0f;
    float outsample1 = 0.0f;
    float wSamp;
    float scaleOutput = 0.7f;
    
    for(int i = 0; i < m_numReaders; i++)
    {
        // update grain duration
        m_readers[i].setDurSamples(( int )floor(m_grainDurSamplesCurrent));
        
        int durSamps = m_readers[i].getDurSamples( );
        
        if(m_readers[i].getActiveState( ) == ACTIVE)
        {
            newSamp = scaleOutput * m_delBuffer[m_readers[i].getCurrSample( )];
        
            // grain window/envelope
            int readerSamp = m_readers[i].getCounter( );
            if(readerSamp < ( float )durSamps / 4.0f)
            {
                // attack (trapezoidal)
                /*
                float m = ( float )4.0f / durSamps;
                float b = 1.0f - m*(durSamps / 4.0f);
                float wSamp = m*readerSamp + b;
                newSamp *= wSamp;
                */
                if(m_snapMode == SNAP_OFF)
                {
                    float attackSamples = ( float )durSamps / 4.0f;
                    float N = attackSamples * 2.0f;
                    wSamp = 0.5f*(1.0f-cos((2.0f*M_PI*readerSamp)/(N - 1)));
                } else
                {
                    // snap is on
                    // trying out a shorter attack length when the snap is on
                    float attackSamples = 64;
                    float N = attackSamples * 2.0f;
                    if(readerSamp < 25)
                    {
                        wSamp = 0.5f*(1.0f-cos((2.0f*M_PI*readerSamp)/(N - 1)));
                    } else
                    {
                        wSamp = 1.0f;
                    }
                }
                m_readers[i].setEnvState(ATTACK);
            } else if(readerSamp >= ( float )3.0f*durSamps / 4.0f)
            {
                // decay (trapezoidal)
                /*
                float m = ( float ) -4.0f / durSamps;
                float b = 1.0f - m*(3.0f*durSamps / 4.0f);
                float wSamp = m*readerSamp + b;
                newSamp *= wSamp;
                 */
                float decaySamples = ( float ) durSamps / 4.0f;
                float N = decaySamples * 2.0f;
                
                int sustainSamples = m_grainDurSamplesCurrent - decaySamples*2;
                int decayCntr = readerSamp - (decaySamples + sustainSamples);
                wSamp = 0.5f*(1.0f+cos((2.0f*M_PI*decayCntr)/(N-1)));
                m_readers[i].setEnvState(DECAY);
            } else
            {
                // sustain otherwise (keep at 1.0)
                wSamp = 1.0f;
                m_readers[i].setEnvState(SUSTAIN);
            }
            
            // multiply sample by window sample
            newSamp *= wSamp;
            
            // increment reader
            m_readers[i].increment( );
        
            // get stereo panning weights
            outsample0 += (newSamp*m_readers[i].getWeight0( ));
            outsample1 += (newSamp*m_readers[i].getWeight1( ));
        }
    }
    
    m_interonsetCounter--;
        
    for(int i = 0; i < m_numReaders; i++)
    {
        if( (m_interonsetCounter <= 0 && m_readers[i].getActiveState( ) == INACTIVE)
           || (m_interonsetCounter <= 0 && m_snapMode == SNAP_ON) )
        {
            m_readers[i].setActiveState(ACTIVE);
            
            // making it random
            //int newStartSample = (rand() % m_counter);
            
            // grab it from delayed part of delay line ...
        
            // calculate the new position in the delayline to take the output
            // currently not doing any interpolation
            float makeRandom = ( float )rand( ) / RAND_MAX;
            if(makeRandom < m_randomness)
            {
                playposfloat = (rand() % m_counter);
            } else
            {
                playposfloat = ( float ) m_writer + (i+1)*m_grainDelaySamples;
            }
            
            // make sure we don't run over the ends of the table
            playpos = ( long )playposfloat;
            //playposfrac = playposfloat - ( float )playpos;
            //playposp1 = playpos + 1;
    
            while(playpos < 0)
                playpos += MAX_DELAY;
            while(playpos >= MAX_DELAY)
                playpos -= MAX_DELAY;
    
            //while(playposp1 < 0)
            //    playposp1 += MAX_DELAY;
            //while(playposp1 >= MAX_DELAY)
            //    playposp1 -= MAX_DELAY;
    
            // look into the delayline and grab the output
            //outsample += scaleOutput*(m_delBuffer[playpos]*(1.0f - playposfrac) + m_delBuffer[playposp1]*playposfrac);
            
            m_readers[i].prepNewChunk(playpos);
            
            // add random weights based on spread parameter to pan grains
            float maxRandVal = m_spread;
            float minRandVal = 0.0f;
            float diff = maxRandVal - minRandVal;
            float weight = ((( float )rand( ) / RAND_MAX) * diff);
            float chance = ( float )rand( ) / RAND_MAX;
            float weight0, weight1;
            if( chance < 0.5 )
            {
                weight0 = 1.0f;
                weight1 = 1.0f - weight;
            } else
            {
                weight0 = 1.0f - weight;
                weight1 = 1.0f;
            }
            
            m_readers[i].setWeight0(weight0);
            m_readers[i].setWeight1(weight1);
            
            // specify whether this grain gets played in reverse or not
            float revChance = ( float )rand( ) / RAND_MAX;
            if(revChance < m_reverse)
            {
                m_readers[i].setForwardRevState( REVERSE );
            } else
            {
                m_readers[i].setForwardRevState( FORWARD );
            }
            
            // choose a new interonset time
            m_interonsetTimeTarget = m_minInteronsetTime + ( (( float )rand( )/RAND_MAX) * (m_maxInteronsetTime - m_minInteronsetTime) );
            m_interonsetSamplesTarget = m_fs*m_interonsetTimeTarget;
            m_interonsetCounter = m_interonsetSamplesTarget;
            break;
        }
    }
    
    
    // --- write into delay  line ---
    
    // write into delay line (add saturation function before)
    float delayInput = *inSamp + 0.5f*(outsample0 + outsample1)*m_feedbackCurrent;
    if(delayInput > 1.0f)
    {
        delayInput = 1.0f;
    } else if(delayInput < -1.0f)
    {
        delayInput = -1.0f;
    } else
    {
        delayInput = 1.5f * delayInput - (delayInput * delayInput * delayInput * .5f);
    }
    m_delBuffer[m_writer] = delayInput;
    
    // decrement
    m_writer--;
    if(m_writer < 0)
    {
        m_writer = MAX_DELAY;
    }
    
    // ---------------------------------
    
    
    float out0 = *inSamp * (1.0f - m_wetdryCurrent) + outsample0*m_wetdryCurrent;
    float out1 = *inSamp * (1.0f - m_wetdryCurrent) + outsample1*m_wetdryCurrent;
    
    // clip
    if(out0 > 1.0f)
    {
        out0 = 1.0f;
    } else if(out0 < -1.0f)
    {
        out0 = -1.0f;
    } else
    {
        out0 = 1.5f * out0 - (out0 * out0 * out0 * .5f);
    }
    if(out1 > 1.0f)
    {
        out1 = 1.0f;
    } else if(out1 < -1.0f)
    {
        out1 = -1.0f;
    } else
    {
        out1 = 1.5f * out1 - (out1 * out1 * out1 * .5f);
    }
        
    *outSamp0 = out0;
    *outSamp1 = out1;
    
    // --------------------------------
    
    if( m_counter < MAX_DELAY )
    {
        m_counter++;
    }
    
}

void DelayLine::clear( )
{
    memset(m_delBuffer, 0.0f, sizeof(float)*MAX_DELAY);
    m_writer = MAX_DELAY-1;
    
}