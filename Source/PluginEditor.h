/*
  ==============================================================================

  This is an automatically generated GUI class created by the Introjucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Introjucer version: 3.1.0

  ------------------------------------------------------------------------------

  The Introjucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright 2004-13 by Raw Material Software Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_4C6F9D4F51C5381B__
#define __JUCE_HEADER_4C6F9D4F51C5381B__

//[Headers]     -- You can add your own extra header files here --
#include "JuceHeader.h"
#include "PluginProcessor.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Introjucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class GrannyGrainAudioProcessorEditor  : public AudioProcessorEditor,
                                         public Timer,
                                         public SliderListener,
                                         public ButtonListener
{
public:
    //==============================================================================
    GrannyGrainAudioProcessorEditor (GrannyGrainAudioProcessor* ownerFilter);
    ~GrannyGrainAudioProcessorEditor();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    void timerCallback();
    GrannyGrainAudioProcessor* getProcessor() const
    {
        return static_cast <GrannyGrainAudioProcessor*>(getAudioProcessor());
    }
    //[/UserMethods]

    void paint (Graphics& g);
    void resized();
    void sliderValueChanged (Slider* sliderThatWasMoved);
    void buttonClicked (Button* buttonThatWasClicked);

    // Binary resources:
    static const char* cropped_jpg;
    static const int cropped_jpgSize;


private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    // these two variables hold the user moved values!
    // so if these values get changed during the snapping stuff, they will only
    // hold onto values that are directly modified by the user on the slider
    float oldMinInteronsetVal, oldMaxInteronsetVal;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<Slider> ampSld;
    ScopedPointer<Label> label2;
    ScopedPointer<Slider> wetDrySld;
    ScopedPointer<Label> label3;
    ScopedPointer<Slider> minInteronsetSld;
    ScopedPointer<Label> label4;
    ScopedPointer<Slider> feedbackSld;
    ScopedPointer<Label> label5;
    ScopedPointer<Slider> grainLengthSld;
    ScopedPointer<Label> label6;
    ScopedPointer<Slider> spreadSld;
    ScopedPointer<Label> label7;
    ScopedPointer<Slider> maxInteronsetSld;
    ScopedPointer<Label> label8;
    ScopedPointer<Slider> grainDelaySld;
    ScopedPointer<Label> label9;
    ScopedPointer<Slider> reverseSld;
    ScopedPointer<Label> label10;
    ScopedPointer<Slider> randomnessSld;
    ScopedPointer<Label> label11;
    ScopedPointer<ToggleButton> oneFourthBtn;
    ScopedPointer<ToggleButton> oneSixthBtn;
    ScopedPointer<ToggleButton> oneEigthBtn;
    ScopedPointer<ToggleButton> oneTwelfthBtn;
    ScopedPointer<ToggleButton> oneSixteenthBtn;
    ScopedPointer<ToggleButton> oneTwentyFourthBtn;
    ScopedPointer<ToggleButton> oneThirtySecondBtn;
    ScopedPointer<ToggleButton> snapOffBtn;
    Image cachedImage_cropped_jpg;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GrannyGrainAudioProcessorEditor)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

#endif   // __JUCE_HEADER_4C6F9D4F51C5381B__
