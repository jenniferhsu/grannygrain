/*
  ==============================================================================

    DelayLine.h
    Created: 23 Nov 2013 5:48:47pm
    Author:  Jennifer Hsu
    
    class that implements a fractional delay line

    right now we're just making it work with sample rate
  ==============================================================================
*/

#ifndef DELAYLINE_H_INCLUDED
#define DELAYLINE_H_INCLUDED

//#include "definitions.h"
#include "Reader.h"


class DelayLine
{
    
public:
    
    // constructors/destructor
    DelayLine( );
    DelayLine( float fs, float delTime );
    ~DelayLine( );
    
    // modify parameters
    void setSampleRate( float fs );
    void setWetDry( float wd );
    void setFeedback( float fb );
    void setDelayTime( float delTime );
    void setGrainLength( float grainLength, float sampleRate);
    void setSpread( float spreadVal );
    void setMinInteronsetTime( float minInteronsetTime );
    void setMaxInteronsetTime( float maxInteronsetTime );
    void setGrainDelay( float grainDelay );
    void setReverse( float reverse );
    void setRandomness( float randomness );
    void setSnap( int snapMode );
    void setInteronsetCounter( int cnt );
    void calculateIncrements( int blockSize );
    
    // retrieve parameters
    float getSampleRate( );
    float getWetDry( );
    float getFeedback( );
    float getDelayTime( );
    
    // tick for samples
    void tick(float * inSamp, float * outSamp0, float *outSamp1);
    
    // clear out the delay line
    void clear( );

private:
    
    float * m_delBuffer;
    float m_fs;
    
    float m_delTimeTarget;
    int m_delSamplesTarget;
    float m_delSamplesCurrent;
    float m_delayIncrement;
    
    float m_grainDurTarget;
    int m_grainDurSamplesTarget;
    float m_grainDurSamplesCurrent;
    float m_grainDurIncrement;
    
    int m_writer;
    Reader * m_readers;
    
    int m_numReaders;
    
    float m_wetdryTarget;
    float m_wetdryCurrent;
    float m_wetdryIncrement;
    
    float m_feedbackTarget;
    float m_feedbackCurrent;
    float m_feedbackIncrement;
    
    float m_spread;
    
    float m_minInteronsetTime;
    float m_maxInteronsetTime;
    
    float m_interonsetTimeTarget;
    int m_interonsetSamplesTarget;
    float m_interonsetSamplesCurrent;
    float m_interonsetSamplesIncrement;
    
    int m_grainDelaySamples;
    
    float m_reverse;
    
    float m_randomness;
    
    int m_snapMode;
    
    int m_interonsetCounter;
    
    int m_counter;
};
    

#endif  // DELAYLINE_H_INCLUDED
