/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
GrannyGrainAudioProcessor::GrannyGrainAudioProcessor()
{
    // initialize UserParams
    UserParams[amp] = 1.0f;
    UserParams[wetDry] = 1.0f;
    UserParams[minInteronset] = 0.75f;
    UserParams[maxInteronset] = 0.75f;
    UserParams[feedback] = 0.0f;
    UserParams[grainLength] = 1.0f;
    UserParams[spread] = 0.0f;
    UserParams[grainDelay] = 0.3f;
    UserParams[reverse] = 0.0f;
    UserParams[randomness] = 0.0f;
    UIUpdateFlag = true;
    
    out = new float[2];
    out[0] = 0.0f;
    out[1] = 0.0f;
    
    // get sample rate will probably return 0 here, so we call it again
    // at the beginning of each block
    delLine = new DelayLine(AudioProcessor::getSampleRate( ), 1.0f);
    
    ampTarget = 1.0f;
    ampCurrent = 1.0f;
    ampIncrement = 0.0f;
    
    lastBar = 1;
    lastBeat = 1;
    lastPpqPos = -1.0f;
    
    snapMode = SNAP_OFF;
    snapValue = 1.0f;
    
}

GrannyGrainAudioProcessor::~GrannyGrainAudioProcessor()
{
    //delete delLine0;
    //delete delLine1;
    delete [ ] out;
    delete delLine;
}

//==============================================================================
const String GrannyGrainAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

int GrannyGrainAudioProcessor::getNumParameters()
{
    // update host with number of parameters
    return totalNumParam;
}

float GrannyGrainAudioProcessor::getParameter (int index)
{
    // update host with parameter values
    switch( index )
    {
        case amp:
            return UserParams[amp];
        case wetDry:
            return UserParams[wetDry];
        case minInteronset:
            return UserParams[minInteronset];
        case maxInteronset:
            return UserParams[maxInteronset];
        case feedback:
            return UserParams[feedback];
        case grainLength:
            return UserParams[grainLength];
        case spread:
            return UserParams[spread];
        case grainDelay:
            return UserParams[grainDelay];
        case reverse:
            return UserParams[reverse];
        case randomness:
            return UserParams[randomness];
        default:
            return 0.0f;
    }
}

void GrannyGrainAudioProcessor::setParameter (int index, float newValue)
{
    // update parameters when the user changes them
    switch( index )
    {
        case amp:
            UserParams[amp] = newValue;
            ampTarget = UserParams[amp];
            break;
        case wetDry:
            UserParams[wetDry] = newValue;
            delLine->setWetDry(UserParams[wetDry]);
            break;
        case minInteronset:
            UserParams[minInteronset] = newValue;
            delLine->setMinInteronsetTime(UserParams[minInteronset]);
            break;
        case maxInteronset:
            UserParams[maxInteronset] = newValue;
            delLine->setMaxInteronsetTime(UserParams[maxInteronset]);
            break;
        case feedback:
            UserParams[feedback] = newValue;
            delLine->setFeedback(UserParams[feedback]);
            break;  
        case grainLength:
            UserParams[grainLength] = newValue;
            delLine->setGrainLength(UserParams[grainLength], sr);
            break;
        case spread:
            UserParams[spread] = newValue;
            delLine->setSpread(UserParams[spread]);
            break;
        case grainDelay:
            UserParams[grainDelay] = newValue;
            delLine->setGrainDelay(UserParams[grainDelay]);
            break;
        case reverse:
            UserParams[reverse] = newValue;
            delLine->setReverse(UserParams[reverse]);
            break;
        case randomness:
            UserParams[randomness] = newValue;
            delLine->setRandomness(UserParams[randomness]);
            break;
        default:
            return;
    }
}

const String GrannyGrainAudioProcessor::getParameterName (int index)
{
    //return String::empty;
    // tell the host our parameter names
    switch( index )
    {
        case amp:
            return "Amplitude";
        case wetDry:
            return "Wet/Dry";
        case minInteronset:
            return "Min Interonset Time";
        case maxInteronset:
            return "Max Interonset Time";
        case feedback:
            return "Feedback";
        case grainLength:
            return "Grain Length";
        case spread:
            return "Spread";
        case grainDelay:
            return "Grain Delay";
        case reverse:
            return "Reverse";
        case randomness:
            return "Randomness";
        default:
            return String::empty;
    }
}

const String GrannyGrainAudioProcessor::getParameterText (int index)
{
    //return String::empty;
    
    // tell the host the parameter values as text
    if(index >= 0 && index < totalNumParam)
    {
        return String(UserParams[index]);
    } else
    {
        return String::empty;
    }
}

const String GrannyGrainAudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String GrannyGrainAudioProcessor::getOutputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

bool GrannyGrainAudioProcessor::isInputChannelStereoPair (int index) const
{
    return true;
}

bool GrannyGrainAudioProcessor::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool GrannyGrainAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool GrannyGrainAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool GrannyGrainAudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double GrannyGrainAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int GrannyGrainAudioProcessor::getNumPrograms()
{
    return 0;
}

int GrannyGrainAudioProcessor::getCurrentProgram()
{
    return 0;
}

void GrannyGrainAudioProcessor::setCurrentProgram (int index)
{
}

const String GrannyGrainAudioProcessor::getProgramName (int index)
{
    return String::empty;
}

void GrannyGrainAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void GrannyGrainAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    delLine->clear( );
}

void GrannyGrainAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    delLine->clear( );
}

void GrannyGrainAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    
    long blockSize = buffer.getNumSamples( );
    
    sr = AudioProcessor::getSampleRate( );
    delLine->setSampleRate(sr);
    delLine->calculateIncrements(blockSize);
    
    ampIncrement = (ampTarget - ampCurrent) / ( float )blockSize;
    
    delLine->setSnap( snapMode );
    
    // if snap mode is on
    int bar, beat;
    if(snapMode == SNAP_ON)
    {
        // get current position information for bpm, num/denom for quantization, and isplaying
        AudioPlayHead::CurrentPositionInfo currPos;
        if (getPlayHead( ) != nullptr && getPlayHead()->getCurrentPosition(currPos))
        {
            // Successfully got the current time from the host..
            lastPosInfo = currPos;
        }
        else
        {
            // If the host fails to fill-in the current time, we'll just clear it to a default..
            lastPosInfo.resetToDefault();
        }
    
        double ppq = lastPosInfo.ppqPosition;
        double beats;
        int numerator = lastPosInfo.timeSigNumerator;
        int denominator = lastPosInfo.timeSigDenominator;
        int ppqPerBar;
        if (numerator == 0 || denominator == 0)
        {
            bar = 1;
            beat = 1;
        } else
        {
            ppqPerBar = (numerator * 4 / denominator);
            // if quarter note:
            // const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 1.0f;
            // if 1/6:
            //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 1.5f;
            // if eigth note:
            // const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 2.0f;
            // if 1/12 note:
            //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 3.0f;
            // if 16th note:
            //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 4.0f;
            // if 1/24:
            //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 6.0f;
            // if 1/32:
            beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * snapValue;
            bar = ((int) ppq) / ppqPerBar + 1;
            beat   = ((int) beats) + 1;
        }
    }
    
    
    // note: I'm only making this work for 2 channels of input and 2 channels of output
    float * chanData0;
    float * chanData1;
    if(buffer.getNumChannels( ) == 1)
    {
        // in case there is only one channel of data, just take the same channel
        // and we'll average it to one value below
        chanData0 = buffer.getSampleData(0);
        chanData1 = buffer.getSampleData(0);
    } else
    {
        chanData0 = buffer.getSampleData(0);
        chanData1 = buffer.getSampleData(1);
    }
    float samp;
    for(long i = 0; i < buffer.getNumSamples( ); i++)
    {
        samp = 0.5f*(chanData0[i]+chanData1[i]);
        
        if(snapMode == SNAP_ON)
        {
            if( bar != lastBar || beat != lastBeat)
            {
                delLine->setInteronsetCounter(0);
                delLine->setMinInteronsetTime(60.0f/lastPosInfo.bpm);
                delLine->setMaxInteronsetTime(60.0f/lastPosInfo.bpm);
            }
        }
        
        ampCurrent += ampIncrement;
        delLine->tick(&samp, &out[0], &out[1]);
        chanData0[i] = ampCurrent * out[0];
        chanData1[i] = ampCurrent * out[1];
    }
    
    if(snapMode == SNAP_ON)
    {
        lastBar = bar;
        lastBeat = beat;
    }
    
    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }
    
}

//==============================================================================
bool GrannyGrainAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* GrannyGrainAudioProcessor::createEditor()
{
    return new GrannyGrainAudioProcessorEditor (this);
}

//==============================================================================
void GrannyGrainAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    
    // save UserParams/Data to file
    XmlElement root("Root");
    XmlElement * el;
    el = root.createNewChildElement("Amplitude");
    el->addTextElement(String(UserParams[amp]));
    el = root.createNewChildElement("WetDry");
    el->addTextElement(String(UserParams[wetDry]));
    el = root.createNewChildElement("MinInteronset");
    el->addTextElement(String(UserParams[minInteronset]));
    el = root.createNewChildElement("MaxInteronset");
    el->addTextElement(String(UserParams[maxInteronset]));
    el = root.createNewChildElement("Feedback");
    el->addTextElement(String(UserParams[feedback]));
    el = root.createNewChildElement("GrainLength");
    el->addTextElement(String(UserParams[grainLength]));
    el = root.createNewChildElement("Spread");
    el->addTextElement(String(UserParams[spread]));
    el = root.createNewChildElement("GrainDelay");
    el->addTextElement(String(UserParams[grainDelay]));
    el = root.createNewChildElement("Reverse");
    el->addTextElement(String(UserParams[reverse]));
    el = root.createNewChildElement("Randomness");
    el->addTextElement(String(UserParams[randomness]));
    copyXmlToBinary(root, destData);
}

void GrannyGrainAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    
    // load UserParams/Data from file
    XmlElement * pRoot = getXmlFromBinary(data, sizeInBytes);
    if(pRoot != NULL)
    {
        forEachXmlChildElement((*pRoot), pChild)
        {
            if(pChild->hasTagName("Amplitude"))
            {
                String text = pChild->getAllSubText( );
                setParameter(amp, text.getFloatValue( ));
            } else if(pChild->hasTagName("WetDry"))
            {
                String text = pChild->getAllSubText( );
                setParameter(wetDry, text.getFloatValue( ));
            } else if(pChild->hasTagName("MinInteronset"))
            {
                String text = pChild->getAllSubText( );
                setParameter(minInteronset, text.getFloatValue( ));
            } else if(pChild->hasTagName("MaxInteronset"))
            {
                String text = pChild->getAllSubText( );
                setParameter(maxInteronset, text.getFloatValue( ));
            } else if(pChild->hasTagName("Feedback"))
            {
                String text = pChild->getAllSubText( );
                setParameter(feedback, text.getFloatValue( ));
            } else if(pChild->hasTagName("GrainLength"))
            {
                String text = pChild->getAllSubText( );
                setParameter(grainLength, text.getFloatValue( ));
            } else if(pChild->hasTagName("Spread"))
            {
                String text = pChild->getAllSubText( );
                setParameter(spread, text.getFloatValue( ));
            } else if(pChild->hasTagName("GrainDelay"))
            {
                String text = pChild->getAllSubText( );
                setParameter(grainDelay, text.getFloatValue( ));
            } else if(pChild->hasTagName("Reverse"))
            {
                String text = pChild->getAllSubText( );
                setParameter(reverse, text.getFloatValue( ));
            } else if(pChild->hasTagName("Randomness"))
            {
                String text = pChild->getAllSubText( );
                setParameter(randomness, text.getFloatValue( ));
            } 
        }
        delete pRoot;
        UIUpdateFlag = true;
    }
}

//==============================================================================
// custom method for toggle buttons
void GrannyGrainAudioProcessor::setSnap(int mode, float value)
{
    snapMode = mode;
    snapValue = value;
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new GrannyGrainAudioProcessor();
}
