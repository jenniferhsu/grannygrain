/*
  ==============================================================================

    definitions.h
    Created: 10 Dec 2013 12:02:05am
    Author:  Jennifer Hsu

  ==============================================================================
*/

#ifndef DEFINITIONS_H_INCLUDED
#define DEFINITIONS_H_INCLUDED

// for Reader class:
#define FORWARD 0
#define REVERSE 1

#define INACTIVE 0
#define ACTIVE 1

#define ATTACK 0
#define SUSTAIN 1
#define DECAY 2

// for DelayLine class:
#define MAX_DELAY 441000    // // maximum delay line length (for now): 10 sec
#define MAX_READERS 100 // is this too many? (we're only using 4 right now)

// for DelayLine and PluginProcessor.cpp:
#define SNAP_OFF 0
#define SNAP_ON 1


#endif  // DEFINITIONS_H_INCLUDED
