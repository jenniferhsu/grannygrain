/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

// my own delay line code
#include "DelayLine.h"


//==============================================================================
/**
*/
class GrannyGrainAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    GrannyGrainAudioProcessor();
    ~GrannyGrainAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock);
    void releaseResources();

    void processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages);

    //==============================================================================
    AudioProcessorEditor* createEditor();
    bool hasEditor() const;

    //==============================================================================
    const String getName() const;

    int getNumParameters();

    float getParameter (int index);
    void setParameter (int index, float newValue);

    const String getParameterName (int index);
    const String getParameterText (int index);

    const String getInputChannelName (int channelIndex) const;
    const String getOutputChannelName (int channelIndex) const;
    bool isInputChannelStereoPair (int index) const;
    bool isOutputChannelStereoPair (int index) const;

    bool acceptsMidi() const;
    bool producesMidi() const;
    bool silenceInProducesSilenceOut() const;
    double getTailLengthSeconds() const;

    //==============================================================================
    int getNumPrograms();
    int getCurrentProgram();
    void setCurrentProgram (int index);
    const String getProgramName (int index);
    void changeProgramName (int index, const String& newName);

    //==============================================================================
    void getStateInformation (MemoryBlock& destData);
    void setStateInformation (const void* data, int sizeInBytes);
    
    // custom methods, parameters and public data
    enum Parameters
    {
        amp = 0,
        wetDry,
        feedback,
        grainLength,
        spread,
        minInteronset,
        maxInteronset,
        grainDelay,
        reverse,
        randomness,
        totalNumParam
    };
    
    // this keeps a copy of the last set of time info that was acquired during an audio
    // callback - the UI component will read this and display it.
    AudioPlayHead::CurrentPositionInfo lastPosInfo;
    
    bool NeedsUIUpdate( ) { return UIUpdateFlag; };
    void ClearUIUpdateFlag( ) { UIUpdateFlag = false; };
    
    void setSnap(int mode, float value);
    

private:
    // private data, helper methods
    float UserParams[totalNumParam];
    bool UIUpdateFlag;
    
    float * out;
    DelayLine * delLine;
    
    float sr;
    
    float ampTarget;
    float ampCurrent;
    float ampIncrement;
    
    float lastBar;
    float lastBeat;
    float lastPpqPos;
    
    int snapMode;
    float snapValue;
    
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GrannyGrainAudioProcessor)
};

#endif  // PLUGINPROCESSOR_H_INCLUDED
