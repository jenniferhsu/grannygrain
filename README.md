grannygrain is a granular synthesis VST

author: Jennifer Hsu (jsh008@ucsd.edu)

This VST allows you to specify:

* length of grains
* min/max interonset times of grains
* randomness of chosen grains in original audio signal
* percentage of grains to be reversed
* feedback in delay line
* whether to snap grains to quanitized beats (1/4, 1/6, 1/8, etc) or not


This code includes the JUCE project and the XCode project to build the VST.  To get to the Xcode project, go to grannygrain/Builds/MacOSX/grannygrain.xcodeproj.  The source code itself is in grannygrain/Source.

* DelayLine.cpp / DelayLine.h: delay line class used to hold samples that will become the 'grains'
* PluginEditor.cpp / PluginEditor.h: GUI code
* PluginProcessor.cpp / PluginProcessor.h: this is where the audio processing takes place
* Reader.cpp / Reader.h: a reader class that allows for easier reading into the delay line
* definitions.h: macros used across all files


